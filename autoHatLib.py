import time
import automationhat
import spidev
from math import fabs

class motor:
	def __init__(self, spiPort):
		self.spiPort = spiPort
		automationhat.enable_auto_lights(False)
		self.spi = spidev.SpiDev()
		self.spi.open(0,0)
		
	def setSpeed(self, speed):
		print ("setting speed")
		if speed > 100:
			speed = 100
		if speed < 0:
			speed = 0
		scaledSpeed = int(speed*550*8/100)
		print (scaledSpeed)
		if scaledSpeed<255:
			spiMSB = 0
			spiLSB = scaledSpeed
		else:
			spiMSB=int(scaledSpeed/256)
			spiLSB=scaledSpeed-(spiMSB*256)
		self.spi.writebytes([spiMSB,spiLSB])
		
	def moveUP(self):
		print("up")
		automationhat.output.one.off()
		automationhat.output.two.on()

	def moveDOWN(self):
		print("down")
		automationhat.output.one.on()
		automationhat.output.two.on()

	def stop(self):
		print("stop")
		automationhat.output.two.off()

	def setDirUp(self):
		automationhat.output.one.off()

	def setDirDown(self):
		automationhat.output.one.on()

	def move(self):
		print("move")
		automationhat.output.two.on()

	def unlock(self):
		automationhat.output.three.on()

	def lock(self):
		automationhat.output.three.off()


class PID:
	
	def __init__(self, p=2.0, i=0.0, d=1.0, derivator=0, integrator=0, integrator_max=50, integrator_min=-50, outMax =100, outMin=-100):

		self.Kp = p
		self.Ki = i
		self.Kd = d
		self.derivator = derivator
		self.integrator = integrator
		self.integrator_max = integrator_max
		self.integrator_min = integrator_min
		self.outMax = outMax
		self.outMin = outMin
		
		self.set_point = 0.0
		self.error = 0.0

		self.p_value = self.Kp * self.error
		self.d_value = self.Kd * (self.error - self.derivator)
		self.i_value = 0.0

	def update(self, current_value):
		
		self.error = self.set_point - current_value
		self.derivator = self.error

		self.integrator = self.integrator + self.error

		if self.integrator > self.integrator_max:
			self.integrator = self.integrator_max
		elif self.integrator < self.integrator_min:
			self.integrator = self.integrator_min

		self.i_value = self.integrator * self.Ki

		pidOut = self.p_value + self.i_value + self.d_value
		if pidOut > self.outMax:
			pidOut = self.outMax
		if pidOut < self.outMin:
			pidOut = self.outMin
		return pidOut

	def setPoint(self, set_point):
		
		self.set_point = set_point
		self.integrator = 0
		self.derivator = 0

	def setIntegrator(self, integrator):
		self.integrator = integrator

	def setDerivator(self, derivator):
		self.derivator = derivator

	def setKp(self, p):
		self.Kp = p

	def setKi(self, i):
		self.Ki = i

	def setKd(self, d):
		self.Kd = d

	def getPoint(self):
		return self.set_point

	def getError(self):
		return self.error

	def getIntegrator(self):
		return self.integrator

	def getDerivator(self):
		return self.derivator


def getWeight(multiplier, offset):
	number = automationhat.analog.one.read()
	weight = (float(number)*multiplier)+offset
	return weight

