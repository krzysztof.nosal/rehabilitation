
# Start with a basic flask app webpage.
from flask_socketio import SocketIO, emit
from flask import Flask, render_template, url_for, copy_current_request_context, request
from random import random
from time import sleep
from threading import Thread, Event
import automationhat
import RPi.GPIO as GPIO
import eventlet
import spidev
from math import fabs

#GPIO.cleanup()
#GPIO.setmode(GPIO.BCM)
#GPIO.setup(18, GPIO.OUT)
#p = GPIO.PWM(18, 1000)
automationhat.enable_auto_lights(False)
speed = 50.0
eventlet.monkey_patch()

spiLSB=0
spiMSB=0
spi = spidev.SpiDev()
#spi.max_speed_hz = 7629
spi.open(0,0)

def spiSetSpeed(speed):
	global spi
	global spiLSB
	global spiMSB
	print ("setting speed")
	print (speed)
	if speed > 100:
		speed = 100
	if speed < 0:
		speed = 0
	scaledSpeed = int(speed*550*8/100)
	print (scaledSpeed)
	if scaledSpeed<255:
		spiMSB = 0
		spiLSB = scaledSpeed
	else:
		spiMSB=int(scaledSpeed/256)
		spiLSB=scaledSpeed-(spiMSB*256)
		print(spiMSB)
		print(spiLSB)
	spi.writebytes([spiMSB,spiLSB])
	
def runPWM():
	global p
	global speed
	p.start(speed)	

def setSpeed():
	global spi
	global speed
	spiSetSpeed(speed)
	#p.ChangeDutyCycle(speed)
	
def stopPWM():
	global p
	p.stop()

def moveUP():
    global speed
    print("up")
    #setSpeed(float(speed))
    automationhat.output.one.off()
    automationhat.output.two.on()

def moveDOWN():
    global speed
    print("down")
    #setSpeed(float(speed-80))
    automationhat.output.one.on()
    automationhat.output.two.on()

def moveSTOP():
    print("stop")
    automationhat.output.one.off()
    automationhat.output.two.off()

	

#
#p=PID(3.0,0.4,1.2)
#p.setPoint(5.0)
#while True:
#     pid = p.update(measurement_value)
#
#


class PID:
	
	def __init__(self, P=2.0, I=0.0, D=1.0, Derivator=0, Integrator=0, Integrator_max=50, Integrator_min=-50, OutMax =100, OutMin=-100):

		self.Kp=P
		self.Ki=I
		self.Kd=D
		self.Derivator=Derivator
		self.Integrator=Integrator
		self.Integrator_max=Integrator_max
		self.Integrator_min=Integrator_min
		self.OutMax = OutMax
		self.OutMin = OutMin
		
		self.set_point=0.0
		self.error=0.0

	def update(self,current_value):
		
		self.error = self.set_point - current_value

		self.P_value = self.Kp * self.error
		self.D_value = self.Kd * ( self.error - self.Derivator)
		self.Derivator = self.error

		self.Integrator = self.Integrator + self.error

		if self.Integrator > self.Integrator_max:
			self.Integrator = self.Integrator_max
		elif self.Integrator < self.Integrator_min:
			self.Integrator = self.Integrator_min

		self.I_value = self.Integrator * self.Ki

		PID = self.P_value + self.I_value + self.D_value
		if PID > self.OutMax:
			PID=OutMax
		if PID < self.OutMin:
			PID=OutMin
		return PID

	def setPoint(self,set_point):
		
		self.set_point = set_point
		self.Integrator=0
		self.Derivator=0

	def setIntegrator(self, Integrator):
		self.Integrator = Integrator

	def setDerivator(self, Derivator):
		self.Derivator = Derivator

	def setKp(self,P):
		self.Kp=P

	def setKi(self,I):
		self.Ki=I

	def setKd(self,D):
		self.Kd=D

	def getPoint(self):
		return self.set_point

	def getError(self):
		return self.error

	def getIntegrator(self):
		return self.Integrator

	def getDerivator(self):
		return self.Derivator
		
		
__author__ = 'Krzysztof Nosal'

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config['DEBUG'] = True

#turn the flask app into a socketio app
socketio = SocketIO(app)

#random number Generator Thread
thread = Thread()
thread_stop_event = Event()

thread2 = Thread()

multiplier = 0.0
offset = 0.0

weight = 1.0
target = 0.0
AutoRunning = 0

#runPWM()
spiSetSpeed(speed)

class RandomThread(Thread):
    def __init__(self):
        self.delay = 1
        super(RandomThread, self).__init__()
        
    def randomNumberGenerator(self):
        global spi
        global multiplier
        global offset
        global speed
        global weight
        global AutoRunning
        spi.open(0,0)
        tablica = []
        #infinite loop of magical random numbers
        print("Making random numbers")
        while AutoRunning==0:
            number = automationhat.analog.one.read()
            numberint=int(number)
            weight = (number*multiplier)+offset
            print("print " + str(weight))
            socketio.emit('newnumber', {'number': weight}, namespace='/test')
            sleep(self.delay)

    def run(self):
        self.randomNumberGenerator()

class AnotherThread(Thread):
	def __init__(self):
		#self.delay = 0.1
		super(AnotherThread, self).__init__()
        
	def randomNumberGenerator2(self):
		global target
		global weight
		global multiplier
		global offset
		global AutoRunning
		global spi
		
		#Histeresis_H_h = 5.0
		#Histeresis_H_l = 0.0
		#Histeresis_L_h = 5.0
		#Histeresis_L_l = 0.0
		spi.open(0,0)
		MovingDown = 0
		MovingUp = 0		
		
		print("started new thread2 " + str(AutoRunning))
		print("target "+str(target) + " weight "+str(weight))
		#tablica = [weight,weight,weight,weight,weight]
		#licznik=0
		#temp = 0.0
		p = PID(P=10.0, I=0.0, D=0.0, Derivator=0, Integrator=0, Integrator_max=50, Integrator_min=-50, OutMax =100, OutMin=-100)
		p.setPoint(weight)
		logFile = open('log.csv', 'w')
		
		while AutoRunning==1:
			number = float(automationhat.analog.one.read())
			numberint=int(number)
			weight = (number*multiplier)+offset
			out = p.update(weight)
			if out < 0:
				dir = 1
				out = out*(-1)
				automationhat.output.one.off()
			else:
				dir = 0 
				automationhat.output.one.on()
			spiSetSpeed(out)
			if	fabs(weight-target) > 3:
				running = 1
				automationhat.output.two.on()
			else:
				running = 0
				automationhat.output.two.off()
				
			logFile.write('{:f} {:f} {:d} {:d} \n'.format(weight ,speed,dir,running ))
			sleep(0.02)
		logfile(close)
	def run(self):
		self.randomNumberGenerator2()

		
@app.route('/', methods=['POST', 'GET'])
def index():
    #only by sending this page first will the client be connected to the socketio instance
    global multiplier
    global offset
    global speed
    global weight
    if request.method == 'POST':
        multiplier = float(request.form['multiplier'])
        offset = float(request.form['offset'])
        speed = float(request.form['speed'])
    print(offset)
    print(multiplier)
    print(speed)
    setSpeed()
		
    return render_template('index.html', multiplier=multiplier, offset=offset, speed=speed, target=target)

@app.route('/up', methods=['POST', 'GET'])
def up():
    moveUP()
    return render_template('index.html', multiplier=multiplier, offset=offset, speed=speed, target=target)

@app.route('/down', methods=['POST', 'GET'])
def down():
    moveDOWN()
    return render_template('index.html', multiplier=multiplier, offset=offset, speed=speed, target=target)
	
@app.route('/stop', methods=['POST', 'GET'])
def stop():
    moveSTOP()
    return render_template('index.html', multiplier=multiplier, offset=offset, speed=speed, target=target)
	
@app.route('/auto', methods=['POST', 'GET'])
def auto():
	global target
	if request.method == 'POST':
		target = float(request.form['target'])
		print(target)
		return render_template('index.html', multiplier=multiplier, offset=offset, speed=speed, target=target)
		
@app.route('/start_auto', methods=['POST', 'GET'])
def start_auto():
    global AutoRunning
    AutoRunning=1
    global thread2
    if not thread2.isAlive():
        print("Starting Thread2")
        thread2 = AnotherThread()
        thread2.start()
    return render_template('index.html', multiplier=multiplier, offset=offset, speed=speed, target = target)

@app.route('/stop_auto', methods=['POST', 'GET'])
def stop_auto():
    global AutoRunning
    AutoRunning=0
    return render_template('index.html', multiplier=multiplier, offset=offset, speed=speed)

@socketio.on('message')
def handle_message(message):
    print('received message: ' + message)
	
@socketio.on('connect')
def test_connect():
    # need visibility of the global thread object
    global thread
    print('Client connected')
    
    if not thread.isAlive():
        print("Starting Thread")
        thread = RandomThread()
        thread.start()
    #Start the random number generator thread only if the thread has not been started before.
    

@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected')


if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0')
