
## Start with a basic flask app webpage.
from flask_socketio import SocketIO  # , emit
from flask import Flask, render_template, request  # , url_for, copy_current_request_context
from time import sleep
import time
from threading import Thread, Event
import eventlet
# from math import fabs
from autoHatLib import motor, PID, getWeight
# import file
# import datetime
eventlet.monkey_patch()

__author__ = 'Krzysztof Nosal'

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config['DEBUG'] = True

socketio = SocketIO(app)

thread = Thread()
thread_stop_event = Event()

speed = 95.0
multiplier = 10.0
offset = 0.0

weight = 1.0
target = 15.0
AutoRunning = 0
m = motor(0)
m.setSpeed(speed)
p = PID(p=10.0, i=0.0, d=0.0, derivator=0, integrator=0, integrator_max=50, integrator_min=-50, outMax=100,
        outMin=-100)
# settings1
controllMethod = 0
# simple Histeresis
simpleHisteresisH = 4.0
simpleHisteresisHSpeed = 95.0
simpleHisteresisL = -4.0
simpleHisteresisLSpeed = 95.0

# double Histeresis
doubleHisteresisH = 3.0
doubleHisteresisHSpeed = 45.0
doubleHisteresisHH = 6.0
doubleHisteresisHHSpeed = 75.0

doubleHisteresisL = -3.0
doubleHisteresisLSpeed = 35.0
doubleHisteresisLL = -6.0
doubleHisteresisLLSpeed = 65.0

# PID
pid_p = 1.0
pid_i = 0.1
pid_d = 0.1
pid_deadZoneH = 2.0
pid_deadZoneL = -2.0

monitorTaskTime = time.time()
controllTaskTime = time.time()

moving = 0

readings = []
max_samples = 30
counter = 10

logging = False
csvFile = 0


class RandomThread(Thread):
    def __init__(self):
        self.delay = 0.005
        super(RandomThread, self).__init__()

    def cyclictaskmanager(self):

        global m
        global p
        global monitorTaskTime
        global controllTaskTime
        global speed
        global multiplier
        global offset

        global weight
        global target
        global AutoRunning

        # settings
        global controllMethod
        # simple Histeresis
        global simpleHisteresisH
        global simpleHisteresisHSpeed
        global simpleHisteresisL
        global simpleHisteresisLSpeed

        # PID
        global pid_p
        global pid_i
        global pid_d
        global pid_deadZoneH
        global pid_deadZoneL

        global moving

        global readings
        global max_samples
        global counter
        global csvFile

        tempweight = 0.0
        stepInterval = 0.05
        firstControllerRun = True
        direction = 0

        print("thread 1 running")
        current_time = time.clock()
        monitorTaskTime = 0.0
        controllTaskTime = 0.0
        print(current_time)
        counter = 0
        m.setSpeed(speed)

        while not thread_stop_event.isSet():
            current_time = time.clock()
            if AutoRunning == 0:
                weight = getWeight(multiplier, offset)

            if current_time - monitorTaskTime > 0.1:
                socketio.emit('newnumber', {'number': weight}, namespace='/test')
                monitorTaskTime = current_time
                print(weight)
                print(current_time)
                print(counter)
                counter = counter+1

            if AutoRunning == 1:
                if len(readings) == max_samples:
                    readings.pop(0)
                if controllMethod == 0:
                    tempweight = getWeight(multiplier, offset)
                    readings.append(tempweight)
                    weight = float(sum(readings) / max(len(readings), 1))
                    difference = weight - target

                    if difference > simpleHisteresisH:
                        m.setSpeed(simpleHisteresisHSpeed)
                        offset = 0
                        if moving==0:
                            m.unlock()
                            time.sleep(0.1)
                        m.moveDOWN()
                        moving = 1
                    if difference < simpleHisteresisL:
                        offset = 0
                        m.setSpeed(simpleHisteresisLSpeed)
                        if moving==0:
                            m.unlock()
                            time.sleep(0.1)
                        m.moveUP()
                        moving = 1
                    if abs(difference) < 1 and moving == 1:
                        m.stop()
                        m.lock()
                        moving = 0
                if controllMethod == 2:
                    pid_result = p.update(weight)
                    if pid_result < 0:
                        m.setSpeed(pid_result*(-1))
                        m.moveUP()
                    else:
                        m.setSpeed(pid_result)
                        m.moveDOWN()
                if controllMethod == 4:
                    if moving == 0:
                        difference = getWeight(multiplier, offset) - target
                        if difference < simpleHisteresisL:
                            controllTaskTime = current_time
                            m.setSpeed(simpleHisteresisLSpeed)
                            m.unlock()
                            time.sleep(0.1)
                            m.moveUP()
                            moving = 1
                            direction = 1
                        if difference > simpleHisteresisH:
                            controllTaskTime = current_time
                            m.setSpeed(simpleHisteresisHSpeed)
                            m.unlock()
                            time.sleep(0.1)
                            m.moveDOWN()
                            moving = 1
                            direction = 0
                    if moving == 1 and current_time-controllTaskTime > stepInterval:
                        m.stop()
                        time.sleep(0.01)
                        #tempweight = getWeight(multiplier, offset)
                        #readings.append(tempweight)
                        #weight = float(sum(readings) / max(len(readings), 1))
                        difference = getWeight(multiplier, offset) - target
                        if difference > simpleHisteresisH and direction == 1:
                            m.setDirDown()
                            m.setSpeed(simpleHisteresisHSpeed)
                            if stepInterval > 0.01:
                                stepInterval = stepInterval - 0.01
                        if difference < simpleHisteresisL and direction == 0:
                            m.setDirUp()
                            m.setSpeed(simpleHisteresisLSpeed)
                            if stepInterval > 0.01:
                                stepInterval = stepInterval - 0.01
                        if abs(difference) < 1:
                            m.stop()
                            time.sleep(0.1)
                            m.lock()
                            stepInterval = 0.05
                            moving = 0
                        else:
                            m.move()
            if logging:
                csvFile.write('{rawvalue};{meanvalue} \n'.format(rawvalue=tempweight, meanvalue=weight))
            sleep(self.delay)

    def run(self):
        self.cyclictaskmanager()


@app.route('/', methods=['POST', 'GET'])
def index():
    # only by sending this page first will the client be connected to the socketio instance
    global m
    global p
    global monitorTaskTime
    global controllTaskTime
    global speed
    global multiplier
    global offset

    global weight
    global target
    global AutoRunning

    # settings
    global controllMethod
    # simple Histeresis
    global simpleHisteresisH
    global simpleHisteresisHSpeed
    global simpleHisteresisL
    global simpleHisteresisLSpeed

    # double Histeresis
    global doubleHisteresisH
    global doubleHisteresisHSpeed
    global doubleHisteresisHH
    global doubleHisteresisHHSpeed

    global doubleHisteresisL
    global doubleHisteresisLSpeed
    global doubleHisteresisLL
    global doubleHisteresisLLSpeed

    # PID
    global pid_p
    global pid_i
    global pid_d
    global pid_deadZoneH
    global pid_deadZoneL

    if request.method == 'POST':
        speed = float(request.form['speed'])
        multiplier = float(request.form['multiplier'])
        offset = float(request.form['offset'])
        weight = float(request.form['weight'])
        target = float(request.form['target'])
        controllMethod = int(request.form['controllMethod'])
        simpleHisteresisH = float(request.form['simpleHisteresisH'])
        simpleHisteresisHSpeed = float(request.form['simpleHisteresisHSpeed'])
        simpleHisteresisL = float(request.form['simpleHisteresisL'])
        simpleHisteresisLSpeed = float(request.form['simpleHisteresisLSpeed'])
        doubleHisteresisH = float(request.form['doubleHisteresisH'])
        doubleHisteresisHSpeed = float(request.form['doubleHisteresisHSpeed'])
        doubleHisteresisHH = float(request.form['doubleHisteresisHH'])
        doubleHisteresisHHSpeed = float(request.form['doubleHisteresisHHSpeed'])
        doubleHisteresisL = float(request.form['doubleHisteresisL'])
        doubleHisteresisLSpeed = float(request.form['doubleHisteresisLSpeed'])
        doubleHisteresisLL = float(request.form['doubleHisteresisLL'])
        doubleHisteresisLLSpeed = float(request.form['doubleHisteresisLLSpeed'])
        pid_p = float(request.form['pid_p'])
        pid_i = float(request.form['pid_i'])
        pid_d = float(request.form['pid_d'])
        pid_deadZoneH = float(request.form['pid_deadZoneH'])
        pid_deadZoneL = float(request.form['pid_deadZoneL'])

    m.setSpeed(speed)
    p.setKp(pid_p)
    p.setKi(pid_i)
    p.setKd(pid_d)
    return render_template('index.html', speed=speed, multiplier=multiplier, offset=offset, weight=weight,
                           target=target, AutoRunning=AutoRunning, controllMethod=controllMethod,
                           simpleHisteresisH=simpleHisteresisH, simpleHisteresisHSpeed=simpleHisteresisHSpeed,
                           simpleHisteresisL=simpleHisteresisL, simpleHisteresisLSpeed=simpleHisteresisLSpeed,
                           doubleHisteresisH=doubleHisteresisH, doubleHisteresisHSpeed=doubleHisteresisHSpeed,
                           doubleHisteresisHH=doubleHisteresisHH, doubleHisteresisHHSpeed=doubleHisteresisHHSpeed,
                           doubleHisteresisL=doubleHisteresisL, doubleHisteresisLSpeed=doubleHisteresisLSpeed,
                           doubleHisteresisLL=doubleHisteresisLL, doubleHisteresisLLSpeed=doubleHisteresisLLSpeed,
                           pid_p=pid_p, pid_i=pid_i, pid_d=pid_d, pid_deadZoneH=pid_deadZoneH,
                           pid_deadZoneL=pid_deadZoneL)


@app.route('/up', methods=['POST', 'GET'])
def up():
    global m
    m.unlock()
    time.sleep(0.1)
    m.moveUP()
    return render_template('controller.html')


@app.route('/down', methods=['POST', 'GET'])
def down():
    global m
    m.unlock()
    time.sleep(0.1)
    m.moveDOWN()
    return render_template('controller.html')


@app.route('/stop', methods=['POST', 'GET'])
def stop():
    global m
    m.stop()
    time.sleep(0.1)
    m.lock()
    m.setDirUp()
    return render_template('controller.html')


@app.route('/start_auto', methods=['POST', 'GET'])
def start_auto():
    global AutoRunning
    AutoRunning = 1

    return render_template('controller.html')


@app.route('/stop_auto', methods=['POST', 'GET'])
def stop_auto():
    global AutoRunning
    global offset
    global m
    m.stop()
    time.sleep(0.1)
    m.lock()
    m.setDirUp()
    AutoRunning = 0
    return render_template('controller.html')


@app.route('/start_log', methods=['POST', 'GET'])
def start_log():
    global logging
    global csvFile

    csvFile = open("log.csv", "w")
    logging = True
    return render_template('controller.html')


@app.route('/stop_log', methods=['POST', 'GET'])
def stop_log():
    global logging
    global csvFile
    csvFile.close()
    logging = False
    return render_template('controller.html')


@app.route('/controller', methods=['POST', 'GET'])
def controller():
    return render_template('controller.html')


@socketio.on('message')
def handle_message(message):
    print('received message: ' + message)


@socketio.on('connect')
def test_connect():
    global thread
    # need visibility of the global thread object
    if not thread.isAlive():
        print("Starting Thread")
        thread = RandomThread()
        thread.start()
    print('Client connected')


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected')


if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0')
